import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kosmos_flutter_app/src/view/auth_checker_view.dart';
import 'package:kosmos_flutter_app/src/view_model/post_view_model.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:riverpod/riverpod.dart' as rv;
import 'package:provider/provider.dart' as pr;

final firebaseInitializerProvider = rv.FutureProvider<FirebaseApp>((ref) async {
  return await Firebase.initializeApp();
});

class App extends ConsumerWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final initialize = ref.watch(firebaseInitializerProvider);
    return pr.MultiProvider(
      providers: [
        pr.ChangeNotifierProvider(create: (context) => UserViewModel()),
        pr.ChangeNotifierProvider(create: (context) => PostViewModel(),)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: initialize.when(
          data: (data) {
            return AuthChecker();
          },
          loading: () => const Scaffold(
            backgroundColor: Colors.white,
          ),
          error: (e, stackTrace) => Scaffold(
            backgroundColor: Colors.redAccent,
          ),
        ),
      ),
    );
  }
}
