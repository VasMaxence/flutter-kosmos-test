import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kosmos_flutter_app/src/view/component/settings_categories.dart';
import 'package:kosmos_flutter_app/src/view/update_account_view.dart';
import 'package:kosmos_flutter_app/src/view/update_credential_view.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

import 'component/box_alert.dart';
import 'component/settings_row.dart';

class AccountView extends StatelessWidget {
  const AccountView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: 27, right: 27, top: 15),
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          child: Icon(
                            Icons.arrow_back_rounded,
                            color: Color(0xff02132B),
                            size: 30,
                          ),
                          onTap: () => Navigator.pop(context),
                        ),
                        GestureDetector(
                          child: Icon(
                            Icons.more_horiz_rounded,
                            color: Color(0xff02132B),
                            size: 30,
                          ),
                          onTap: () async {
                            showCupertinoModalPopup<void>(
                              context: context,
                              builder: (BuildContext context) =>
                                  CupertinoActionSheet(
                                title: Text('Que souhaitez-vous faire ?'),
                                cancelButton: CupertinoActionSheetAction(
                                  child: Text('Annuler'),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                actions: [
                                  CupertinoActionSheetAction(
                                    child: Text('Se déconnecter'),
                                    onPressed: () async {
                                      pr.Provider.of<UserViewModel>(context,
                                              listen: false)
                                          .signOut();
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    },
                                    isDestructiveAction: true,
                                  ),
                                  CupertinoActionSheetAction(
                                    child: Text('Supprimer mon compte'),
                                    isDestructiveAction: true,
                                    onPressed: () async {
                                      if (await _deleteAccount(context)) {
                                        pr.Provider.of<UserViewModel>(context,
                                                listen: false)
                                            .deleteAccount(context);
                                      }
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    },
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Column(
                        children: [
                          pr.Consumer<UserViewModel>(
                            builder: (context, user, _) {
                              if (user.user == null) return Container();
                              return Column(
                                children: [
                                  Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      Container(
                                        width: 92,
                                        height: 92,
                                        clipBehavior: Clip.hardEdge,
                                        decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(.2),
                                          borderRadius:
                                              BorderRadius.circular(100),
                                        ),
                                        child: Image.network(
                                          user.user!.photoURL ?? '',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        child: GestureDetector(
                                          child: Container(
                                            width: 22,
                                            height: 22,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              color:
                                                  Colors.black.withOpacity(.7),
                                            ),
                                            child: Icon(
                                              Icons.mode_edit_outline_rounded,
                                              size: 10,
                                              color: Colors.white,
                                            ),
                                          ),
                                          onTap: () async =>
                                              await _updateProfilePicture(
                                                  context),
                                        ),
                                        bottom: -11,
                                        left: 35,
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 15),
                                    child: Text(
                                      "${user.user!.displayName}",
                                      overflow: TextOverflow.clip,
                                      maxLines: 2,
                                      style: TextStyle(
                                        color: Color(0xff02132B),
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "${user.user!.email}",
                                    overflow: TextOverflow.clip,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: Color(0xffA7ADB5),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              );
                            },
                          )
                        ],
                      ),
                    ),
                    Divider(
                      color: Color(0xff02132B).withOpacity(.3),
                    ),
                  ],
                ),
                pr.Consumer<UserViewModel>(
                  builder: (context, user, _) {
                    if (user.user == null) return Container();
                    return SettingsCategories(settings: [
                      SettingsRow(
                        name: '${user.user!.displayName}',
                        desc: user.user!.email!,
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UpdateAccountView())),
                        prefix: Image.network(
                          user.user!.photoURL ?? '',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SettingsRow(
                        name: 'Sécurité',
                        desc: 'Mot de passe, email...',
                        prefix: Icon(
                          Icons.lock_outline_rounded,
                          color: Colors.white,
                          size: 18,
                        ),
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UpdateCredentialView())),
                      ),
                    ], name: 'Mon Compte');
                  },
                ),
                SettingsCategories(settings: [
                  SettingsRow(
                    name: 'Notifications push',
                    desc: 'Activées',
                    onTap: () => null,
                    prefix: Icon(
                      Icons.notifications_outlined,
                      color: Colors.white,
                      size: 18,
                    ),
                    isSwitcher: true,
                    defaultActivated: true,
                  ),
                ], name: 'Paramètres'),
                SettingsCategories(settings: [
                  SettingsRow(
                    name: 'Aide',
                    desc: 'Contactez nous par email.',
                    onTap: () => null,
                    prefix: Icon(
                      Icons.help_outline_rounded,
                      size: 18,
                      color: Colors.white,
                    ),
                  ),
                  SettingsRow(
                    name: 'Partager l\'app',
                    desc: 'Soutenez-nous en partageant l\'app.',
                    prefix: Icon(
                      Icons.lock_outline_rounded,
                      color: Colors.white,
                      size: 18,
                    ),
                    onTap: () => null,
                  ),
                ], name: 'Autres'),
                SettingsCategories(settings: [
                  SettingsRow(
                    name: 'Politique de confidentialité',
                    desc: '',
                    onTap: () => null,
                  ),
                  SettingsRow(
                    name: 'Conditions générales de ventes et d\'utilisations',
                    desc: '',
                    onTap: () => null,
                  ),
                  SettingsRow(
                    name: 'Mentions légales',
                    desc: '',
                    onTap: () => null,
                  ),
                  SettingsRow(
                    name: 'À propos',
                    desc: '',
                    onTap: () => null,
                  ),
                ], name: 'Liens'),
                SettingsCategories(settings: [
                  SettingsRow(
                    name: 'Notre page Facebook',
                    desc: '',
                    onTap: () => null,
                    prefix: Image.network(
                      'https://cdn.icon-icons.com/icons2/2429/PNG/512/facebook_logo_icon_147291.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  SettingsRow(
                    name: 'Notre Instagram',
                    desc: '',
                    onTap: () => null,
                    prefix: Image.network(
                      'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/2048px-Instagram_icon.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  SettingsRow(
                    name: 'Notre Twitter',
                    desc: '',
                    onTap: () => null,
                    prefix: Image.network(
                      'https://f.hellowork.com/blogdumoderateur/2019/11/twitter-logo.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  SettingsRow(
                    name: 'Notre Snapchat',
                    desc: '',
                    onTap: () => null,
                    prefix: Image.network(
                      'https://www.01net.com/i/0/0/c07908e7/fa8090f612d1b5a1d27c06cb.jpeg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ], name: 'Réseaux sociaux'),
                Center(
                  child: RichText(
                    text: TextSpan(
                        text: 'Édité par ',
                        style: TextStyle(
                          color: Color(0xff02132B),
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                        children: [
                          TextSpan(
                              text: 'Kosmos-Digital.com',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                              )),
                        ]),
                    overflow: TextOverflow.clip,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _deleteAccount(BuildContext context) async {
    bool tmp = false;
    await showDialog(
      context: context,
      builder: (ctx) => BoxAlert(
        title: 'Supprimer mon compte',
        content: 'Souhaitez-vous vraiment supprimer votre compte ?',
        actions: [
          ActionButton(
              name: 'Oui',
              onTap: () {
                tmp = true;
                Navigator.pop(context);
              }),
          ActionButton(
              name: 'Non',
              onTap: () {
                tmp = false;
                Navigator.pop(context);
              }),
        ],
      ),
    );
    return tmp;
  }

  Future _updateProfilePicture(BuildContext context) async {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text('Que souhaitez-vous faire ?'),
        cancelButton: CupertinoActionSheetAction(
          child: Text('Annuler'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          CupertinoActionSheetAction(
            child: Text('Prendre une photo/vidéo'),
            onPressed: () async {
              await newProfileImage(context, source: ImageSource.camera);
              Navigator.pop(context);
            },
          ),
          CupertinoActionSheetAction(
            child: Text('Bibliothèque photo/vidéo'),
            onPressed: () async {
              await newProfileImage(context);
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }

  Future<bool> newProfileImage(BuildContext context,
      {ImageSource source = ImageSource.gallery}) async {
    ImagePicker imagePicker = ImagePicker();
    XFile? file;

    file = await imagePicker.pickImage(source: source);
    await pr.Provider.of<UserViewModel>(context, listen: false)
        .updateUserInformation(context, image: file);
    return true;
  }
}
