import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view_model/post_view_model.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

class PostView extends StatelessWidget {
  final Map<String, dynamic> data;

  PostView({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Hero(
                tag: data["name"],
                child: Image.network(
                  data["image"],
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SafeArea(
              child: Stack(
                children: [
                  Positioned(
                    top: 15,
                    left: 15,
                    child: Container(
                      width: 42,
                      height: 42,
                      decoration: BoxDecoration(
                        color: Colors.black26,
                        borderRadius: BorderRadius.circular(42),
                      ),
                      child: GestureDetector(
                        child: Icon(
                          Icons.arrow_back_rounded,
                          color: Color(0xff88888B),
                          size: 30,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 15,
                    right: 15,
                    child: GestureDetector(
                      onTap: () async {
                        showCupertinoModalPopup<void>(
                          context: context,
                          builder: (BuildContext context) =>
                              CupertinoActionSheet(
                            title: Text('Que souhaitez-vous faire ?'),
                            cancelButton: CupertinoActionSheetAction(
                              child: Text('Annuler'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            actions: [
                              data["user"]["user_id"] ==
                                      pr.Provider.of<UserViewModel>(context,
                                              listen: false)
                                          .user
                                          ?.uid
                                  ? CupertinoActionSheetAction(
                                      child: Text('Supprimer la publication'),
                                      onPressed: () async {
                                        pr.Provider.of<PostViewModel>(context,
                                                listen: false)
                                            .deletePost(data["id"]);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      },
                                      isDestructiveAction: true,
                                    )
                                  : CupertinoActionSheetAction(
                                      child: Text('Signaler la publication'),
                                      onPressed: () async {
                                        Navigator.pop(context);
                                      },
                                      isDestructiveAction: true,
                                    ),
                            ],
                          ),
                        );
                      },
                      child: Container(
                        height: 42,
                        width: 42,
                        decoration: BoxDecoration(
                          color: Colors.black26,
                          borderRadius: BorderRadius.circular(42),
                        ),
                        child: Icon(
                          Icons.more_vert_rounded,
                          color: Color(0xff88888B),
                          size: 30,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .4,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [Colors.black, Colors.transparent],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * .05,
              left: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      clipBehavior: Clip.hardEdge,
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.white),
                      child: Image.network(
                        data["user"]["user_image"],
                        fit: BoxFit.cover,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                (data["user"]["user_name"] as String)
                                    .toUpperCase(),
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text(
                                  data["posted_at"].toString(),
                                  maxLines: 1,
                                  overflow: TextOverflow.clip,
                                  style: TextStyle(
                                    fontSize: 11,
                                    color: Color(0xff88888B),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .8,
                            child: Text(
                              data["desc"],
                              maxLines: 1,
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                fontSize: 11,
                                color: Color(0xff88888B),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
