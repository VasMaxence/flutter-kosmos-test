import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:kosmos_flutter_app/src/view/component/box_alert.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_button.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_textfield.dart';
import 'package:kosmos_flutter_app/src/view/create_account_view.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';

import 'package:provider/provider.dart' as pr;

class LoginView extends StatelessWidget {
  final bool canPopPage;

  final _formKey = GlobalKey<FormState>();
  final _formResetPasswordKey = GlobalKey<FormState>();
  final _emailResetPasswordKey = TextEditingController();
  final _emailKey = TextEditingController();
  final _passwordKey = TextEditingController();

  LoginView({
    Key? key,
    this.canPopPage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  canPopPage
                      ? Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: GestureDetector(
                              child: Icon(
                                Icons.arrow_back_rounded,
                                color: Color(0xff02132B),
                                size: 30,
                              ),
                              onTap: () => Navigator.pop(context),
                            ),
                          ),
                        )
                      : Container(),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 50, horizontal: 40),
                    child: Text(
                      'Connectez-vous ou créez un compte',
                      maxLines: 3,
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 23,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        color: Color(0xff02132B),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SampleTextFormField(
                            textController: _emailKey,
                            name: 'Email',
                            hint: 'john.doe@gmail.com',
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            validator: (String? value) {
                              if (value == null ||
                                  !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value))
                                return 'Email incorrect...';
                              return null;
                            },
                          ),
                          SampleTextFormField(
                            textController: _passwordKey,
                            name: 'Mot de passe',
                            hint: 'Mot de passe',
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            isSecretInput: true,
                            textInputAction: TextInputAction.done,
                            validator: (String? value) =>
                                (value != null && value.length > 5)
                                    ? null
                                    : 'Veuillez vérifier votre mot de passe...',
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 7),
                            child: GestureDetector(
                              onTap: () =>
                                  _showResetPasswordModal(context),
                              child: Text.rich(
                                TextSpan(
                                  text: 'Mot de passe oublié ? ',
                                  style: TextStyle(color: Color(0xffA7ADB5)),
                                  children: [
                                    TextSpan(
                                      text: 'Réinitialisez le.',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        decoration: TextDecoration.underline,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SampleButton(
                              content: 'Valider',
                              onTap: () async {
                                if (_formKey.currentState!.validate()) {
                                  pr.Provider.of<UserViewModel>(context,
                                          listen: false)
                                      .signInWithEmailAndPassword(
                                          _emailKey.value.text,
                                          _passwordKey.value.text,
                                          context);
                                }
                              }),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 17),
                            child: GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CreateAccountView(
                                            canPopPage: true,
                                          ))),
                              child: Text.rich(
                                TextSpan(
                                  text: 'Pas encore de compte ? ',
                                  style: TextStyle(color: Color(0xffA7ADB5)),
                                  children: [
                                    TextSpan(
                                      text: 'Créez en un.',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        decoration: TextDecoration.underline,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      ),
    );
  }

  Future _showResetPasswordModal(BuildContext context) async {
    await showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(55),
        ),
        isScrollControlled: true,
        builder: (context) {
          return Container(
            constraints: BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height * .55,
              minHeight: MediaQuery.of(context).size.height * .45,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * .2,
                    height: 2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2),
                      color: Colors.grey,
                    ),
                  ),
                  Text(
                    'Réinitialisez votre mot de passe',
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 23,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      color: Color(0xff02132B),
                    ),
                  ),
                  Text(
                    "Entrez l’adresse email associée à votre compte. Nous vous enverrons un email de réinitialisation sur celle-ci.",
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      color: Color(0xffA7ADB5),
                    ),
                  ),
                  Form(
                    key: _formResetPasswordKey,
                    child: Column(
                      children: [
                        SampleTextFormField(
                          textController: _emailResetPasswordKey,
                          name: 'Email',
                          hint: 'john.doe@gmail.com',
                          backgroundColor: Color(0xff02132B).withOpacity(.03),
                          textInputAction: TextInputAction.done,
                          validator: (String? value) {
                            if (value == null ||
                                value.isEmpty ||
                                !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value)) {
                              return "Vérifiez votre adresse email...";
                            }
                            return null;
                          },
                        ),
                        SampleButton(
                            content: 'Valider',
                            paddingTop: 40,
                            onTap: () async {
                              if (_formResetPasswordKey.currentState!
                                  .validate()) {
                                pr.Provider.of<UserViewModel>(context, listen: false)
                                    .resetPassword(
                                        _emailResetPasswordKey.value.text,
                                        context);
                                Navigator.pop(context, true);
                                showDialog(
                                  context: context,
                                  builder: (ctx) => BoxAlert(
                                      title: 'Vérifiez votre boîte mail',
                                      content:
                                          'Vous avez reçu un email afin de réinitialiser votre mot de passe.'),
                                );
                              }
                            }),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
