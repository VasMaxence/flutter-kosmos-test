import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kosmos_flutter_app/src/view_model/auth_provider.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

import 'component/box_alert.dart';
import 'component/sample_button.dart';
import 'component/sample_textfield.dart';
import 'component/settings_row.dart';

class UpdateCredentialView extends StatelessWidget {
  const UpdateCredentialView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Stack(
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Color(0xff02132B),
                          size: 24,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                      Center(
                        child: Text(
                          "Modifier",
                          overflow: TextOverflow.clip,
                          maxLines: 2,
                          style: TextStyle(
                            color: Color(0xff02132B),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                pr.Consumer<UserViewModel>(
                  builder: (context, user, _) {
                    return Column(
                      children: [
                        SettingsRow(
                          name: 'Adresse email',
                          desc: '${user.user!.email}',
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EmailChanger())),
                        ),
                        SettingsRow(
                          name: 'Mot de passe',
                          desc: 'Dernière modification: il y a jours',
                          //TODO last modify
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PasswordChanger())),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class EmailChanger extends StatelessWidget {
  EmailChanger({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Stack(
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Color(0xff02132B),
                          size: 24,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                      Center(
                        child: Text(
                          "Adresse Email",
                          overflow: TextOverflow.clip,
                          maxLines: 2,
                          style: TextStyle(
                            color: Color(0xff02132B),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      pr.Consumer<UserViewModel>(
                        builder: (context, user, _) {
                          return SampleTextFormField(
                            name: 'Email*',
                            hint: 'John.doe@gmail.com',
                            textInputAction: TextInputAction.done,
                            initialValue: user.user!.email,
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            validator: (String? email) {
                              if (email == null || email.isEmpty) {
                                return "Obligatoire";
                              }
                              pr.Provider.of<UserViewModel>(context,
                                      listen: false)
                                  .updateUserInformation(context, email: email)
                                  .then((value) {
                                if (value) {
                                  showDialog(
                                    context: context,
                                    builder: (ctx) => BoxAlert(
                                        title: 'Confirmer votre adresse email',
                                        content:
                                            "Vous venez de recevoir un email de vérification sur $email"),
                                  );
                                }
                              });
                            },
                          );
                        },
                      ),
                      SampleButton(
                        content: 'Valider',
                        onTap: () async {
                          if (_formKey.currentState!.validate()) {}
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Text(
                          '* Les champs marqués sont obligatoires.',
                          maxLines: 2,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            color: Color(0xff02132B),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PasswordChanger extends ConsumerWidget {
  PasswordChanger({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final _passwordKey = TextEditingController();
  final _oldPasswordKey = TextEditingController();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Stack(
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Color(0xff02132B),
                          size: 24,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                      Center(
                        child: Text(
                          "Mot de passe",
                          overflow: TextOverflow.clip,
                          maxLines: 2,
                          style: TextStyle(
                            color: Color(0xff02132B),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      pr.Consumer<UserViewModel>(
                        builder: (context, user, _) {
                          return SampleTextFormField(
                            name: 'Mot de passe*',
                            hint: 'Mot de passe',
                            textController: _oldPasswordKey,
                            textInputAction: TextInputAction.next,
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            isSecretInput: true,
                            validator: (String? password) {
                              if (password == null || password.isEmpty) {
                                return "Obligatoire";
                              }
                            },
                          );
                        },
                      ),
                      SampleTextFormField(
                        textController: _passwordKey,
                        name: 'Nouveau mot de passe*',
                        hint: 'Nouveau mot de passe',
                        textInputAction: TextInputAction.next,
                        backgroundColor: Color(0xff02132B).withOpacity(.03),
                        isSecretInput: true,
                        validator: (String? password) {
                          if (password == null || password.isEmpty) {
                            return "Obligatoire";
                          }
                        },
                      ),
                      SampleTextFormField(
                        name: 'Répétez votre nouveau mot de passe*',
                        hint: 'Répétez votre nouveau mot de passe',
                        textInputAction: TextInputAction.done,
                        backgroundColor: Color(0xff02132B).withOpacity(.03),
                        isSecretInput: true,
                        validator: (String? password) {
                          if (password == null || password.isEmpty) {
                            return "Obligatoire";
                          } else if (_passwordKey.value.text != password) {
                            return "Mot de passe différent";
                          }
                        },
                      ),
                      SampleButton(
                        content: 'Valider',
                        onTap: () async {
                          if (_formKey.currentState!.validate() &&
                              await ref
                                  .watch(authenticationProvider)
                                  .checkSamePassword(
                                      _oldPasswordKey.value.text)) {
                            pr.Provider.of<UserViewModel>(context,
                                    listen: false)
                                .updateUserInformation(context,
                                    password: _passwordKey.value.text)
                                .then((value) => Navigator.pop(context));
                          } else {
                            showDialog(
                              context: context,
                              builder: (ctx) => BoxAlert(
                                  title: 'Mot de passe incorrect',
                                  content: "Veuillez vérifiez vos données."),
                            );
                          }
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Text(
                          '* Les champs marqués sont obligatoires.',
                          maxLines: 2,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            color: Color(0xff02132B),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
