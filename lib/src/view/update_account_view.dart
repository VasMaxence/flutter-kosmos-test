import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kosmos_flutter_app/src/view_model/auth_provider.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

import 'component/sample_button.dart';
import 'component/sample_textfield.dart';
import 'component/settings_row.dart';

class UpdateAccountView extends StatelessWidget {
  const UpdateAccountView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Stack(
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Color(0xff02132B),
                          size: 24,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                      Center(
                        child: Text(
                          "Modifier",
                          overflow: TextOverflow.clip,
                          maxLines: 2,
                          style: TextStyle(
                            color: Color(0xff02132B),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SettingsRow(
                  name: 'Informations personnelles',
                  desc: 'Nom, prénom, date de naissance...',
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UpdatePersonalDataView())),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class UpdatePersonalDataView extends StatelessWidget {
  UpdatePersonalDataView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Stack(
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Color(0xff02132B),
                          size: 24,
                        ),
                        onTap: () => Navigator.pop(context),
                      ),
                      Center(
                        child: Text(
                          "Informations personnelles",
                          overflow: TextOverflow.clip,
                          maxLines: 2,
                          style: TextStyle(
                            color: Color(0xff02132B),
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      pr.Consumer<UserViewModel>(
                        builder: (context, user, _) {
                          return SampleTextFormField(
                            name: 'Prénom*',
                            hint: 'John',
                            textInputAction: TextInputAction.done,
                            initialValue: user.user!.displayName,
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Obligatoire";
                              }
                              pr.Provider.of<UserViewModel>(context,
                                      listen: false)
                                  .updateUserInformation(context,
                                      firstname: value)
                                  .then((value) {
                                Navigator.pop(context);
                              });
                            },
                          );
                        },
                      ),
                      SampleButton(
                        content: 'Valider',
                        onTap: () async {
                          if (_formKey.currentState!.validate()) {}
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Text(
                          '* Les champs marqués sont obligatoires.',
                          maxLines: 2,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            color: Color(0xff02132B),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
