import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kosmos_flutter_app/src/view/component/custom_image_picker.dart';
import 'package:kosmos_flutter_app/src/view/component/waiting_button.dart';
import 'package:kosmos_flutter_app/src/view_model/post_view_model.dart';
import 'package:provider/provider.dart' as pr;

import 'component/box_alert.dart';

class AddingPostView extends StatefulWidget {
  const AddingPostView({Key? key}) : super(key: key);

  @override
  _AddingPostViewState createState() => _AddingPostViewState();
}

class _AddingPostViewState extends State<AddingPostView> {
  final submitButtonKey = GlobalKey<WaitingButtonState>();
  final pickerKey = GlobalKey<CustomImagePickerState>();

  bool hasImagePick = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      child: Icon(
                        Icons.arrow_back_rounded,
                        color: Color(0xff02132B),
                        size: 30,
                      ),
                      onTap: () => Navigator.pop(context),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Choisissez une image",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: TextStyle(
                          color: Color(0xff02132B),
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 4,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    CustomImagePicker(
                      key: pickerKey,
                      onTap: () {
                        submitButtonKey.currentState?.updateState();
                        setState(() => hasImagePick = true);
                      },
                    ),
                    hasImagePick
                        ? Positioned(
                            top: -10,
                            right: -10,
                            child: GestureDetector(
                              child: Stack(
                                children: [
                                  Container(
                                    width: 34,
                                    height: 34,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(34)),
                                  ),
                                  Positioned(
                                    top: 2,
                                    left: 2,
                                    child: Container(
                                      width: 30,
                                      height: 30,
                                      decoration: BoxDecoration(
                                        color: Colors.redAccent,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.delete_outline_rounded,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              onTap: () {
                                pickerKey.currentState?.resetPicker();
                                submitButtonKey.currentState?.deleteState();
                                setState(() => hasImagePick = false);
                              },
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: WaitingButton(
                  key: submitButtonKey,
                  content: 'Suivant',
                  defaultActive: hasImagePick,
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddingPostDescView(
                              image: pickerKey.currentState!.file!))),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AddingPostDescView extends StatefulWidget {
  final XFile image;

  AddingPostDescView({
    Key? key,
    required this.image,
  }) : super(key: key);

  @override
  _AddingPostDescViewState createState() => _AddingPostDescViewState();
}

class _AddingPostDescViewState extends State<AddingPostDescView> {
  final submitButtonKey = GlobalKey<WaitingButtonState>();
  final descController = TextEditingController();

  @override
  void initState() {
    descController.addListener(() {
      if (descController.value.text.isNotEmpty)
        submitButtonKey.currentState?.updateState();
    });
    super.initState();
  }

  @override
  void dispose() {
    descController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: SingleChildScrollView(
                physics: ClampingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          child: Icon(
                            Icons.arrow_back_rounded,
                            color: Color(0xff02132B),
                            size: 30,
                          ),
                          onTap: () => Navigator.pop(context),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            "Ajouter une description",
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: TextStyle(
                              color: Color(0xff02132B),
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * .6,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Color(0xffF7F7F8),
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: TextField(
                        controller: descController,
                        minLines: 1,
                        maxLines: 20,
                        keyboardType: TextInputType.multiline,
                        maxLength: 512,
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 8),
                          hintText: 'Décrivez votre post...',
                          counterText: '',
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    WaitingButton(
                      key: submitButtonKey,
                      content: 'Suivant',
                      onTap: () async {
                        if (await pr.Provider.of<PostViewModel>(context,
                                listen: false)
                            .addNewPost(
                                widget.image, descController.value.text)) {
                          Navigator.pop(context);
                          Navigator.pop(context, true);
                        } else {
                          await showDialog(
                            context: context,
                            builder: (ctx) =>
                                BoxAlert(title: 'Une erreur est survenue', content: 'Veuillez réessayer s\'il vous plait.'),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
