import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_button.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_checker.dart';

class CGUModalView extends StatefulWidget {
  const CGUModalView({Key? key}) : super(key: key);

  @override
  _CGUModalViewState createState() => _CGUModalViewState();
}

class _CGUModalViewState extends State<CGUModalView> {
  final _formKey = GlobalKey<FormState>();
  final _checkerKey = GlobalKey<SampleCheckerState>();

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: MediaQuery.of(context).size.height * .8,
        minHeight: MediaQuery.of(context).size.height * .75,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        child: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .2,
                constraints: BoxConstraints(maxHeight: 3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2),
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "CGVU et politique de confidentalité",
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Color(0xff02132B),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              maxHeight:
                                  MediaQuery.of(context).size.height * .4),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Text(
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis ex ex, nec pretium ante mollis id. Nullam lorem magna, malesuada sit amet nisi ut, congue lobortis turpis. Nulla pellentesque libero vitae mollis facilisis. Nulla auctor diam posuere aliquam scelerisque. Curabitur id sodales diam. Aliquam ut bibendum mi. Proin id ipsum sed nisl commodo dapibus. Aliquam eleifend mollis ipsum, vel rhoncus mauris. In a neque a urna vulputate elementum non sed quam. Ut in faucibus ante.Pellentesque non dolor consectetur, mollis nunc id, tincidunt orci. Suspendisse accumsan odio nec nunc mattis maximus. Donec id varius orci, ut eleifend metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse potenti. In non sagittis nunc. In lectus ipsum, suscipit vitae iaculis a, sagittis eu mauris. Nam at finibus eros. Sed congue efficitur quam, ultricies rutrum diam accumsan quis. Maecenas finibus ex eu efficitur maximus. Vivamus in erat laoreet, malesuada mi ut, suscipit tortor. Ut euismod iaculis velit, et euismod dolor semper ac. Aliquam bibendum, nulla ac semper volutpat, arcu urna sagittis nulla, non vehicula ante neque eu velit. Duis a pharetra felis. Donec sed luctus metus. Duis feugiat risus eu tortor tempus, id fermentum felis dictum.",
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                fontSize: 14,
                                color: Color(0xff02132B).withOpacity(.35),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              SampleChecker(
                                  key: _checkerKey,
                                  content:
                                      'J’accepte la politique de confidentialité'
                                      ' et lesconditions générales de ventes et d’utilisation.'),
                              SampleButton(
                                content: 'Continuer',
                                onTap: () async {
                                  if (!(_checkerKey.currentState?.state ??
                                      false)) {
                                    _checkerKey.currentState?.mustAcceptCGU();
                                    return;
                                  }
                                  Navigator.of(context).pop(true);
                                },
                                paddingTop: 15,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
