import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/home_view.dart';
import 'package:kosmos_flutter_app/src/view/login_view.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

class AuthChecker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return pr.Consumer<UserViewModel>(
      builder: (context, user, _) {
        return (user.user != null) ? HomeView() : LoginView();
      },
    );
  }
}
