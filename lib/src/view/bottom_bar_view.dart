import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/account_view.dart';

class BottomBarView extends StatefulWidget {
  const BottomBarView({Key? key}) : super(key: key);

  @override
  _BottomBarViewState createState() => _BottomBarViewState();
}

class _BottomBarViewState extends State<BottomBarView> {
  int active = 0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .3,
              child: Center(
                child: Icon(
                  Icons.home,
                ),
              ),
            ),
            onTap: () => setState(() => active = 0),
          ),
          GestureDetector(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * .3,
                child: Center(
                  child: Icon(
                    Icons.settings,
                  ),
                ),
              ),
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AccountView()))),
        ],
      ),
    );
  }
}
