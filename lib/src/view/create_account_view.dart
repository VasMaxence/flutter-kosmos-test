import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kosmos_flutter_app/src/view/cgu_modal_view.dart';
import 'package:kosmos_flutter_app/src/view/component/box_alert.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_button.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_textfield.dart';
import 'package:kosmos_flutter_app/src/view/create_profile_view.dart';
import 'package:kosmos_flutter_app/src/view_model/auth_provider.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

class CreateAccountView extends StatelessWidget {
  final bool canPopPage;

  final _formKey = GlobalKey<FormState>();
  final _emailKey = TextEditingController();
  final _passwordKey = TextEditingController();
  final _passwordRepeatKey = TextEditingController();

  CreateAccountView({
    Key? key,
    this.canPopPage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  canPopPage
                      ? Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: GestureDetector(
                              child: Icon(
                                Icons.arrow_back_rounded,
                                color: Color(0xff02132B),
                                size: 30,
                              ),
                              onTap: () => Navigator.pop(context),
                            ),
                          ),
                        )
                      : Container(),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 50, horizontal: 40),
                    child: Text(
                      'Créez un compte',
                      maxLines: 3,
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 23,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        color: Color(0xff02132B),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SampleTextFormField(
                            textController: _emailKey,
                            name: 'Email',
                            hint: 'john.doe@gmail.com',
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            validator: (String? value) {
                              if (value == null ||
                                  !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value))
                                return 'Email incorrect...';
                              return null;
                            },
                          ),
                          SampleTextFormField(
                            textController: _passwordKey,
                            name: 'Mot de passe',
                            hint: 'Mot de passe',
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            isSecretInput: true,
                            textInputAction: TextInputAction.next,
                            validator: (String? value) =>
                                (value != null && value.length > 5)
                                    ? null
                                    : 'Veuillez vérifier votre mot de passe...',
                          ),
                          SampleTextFormField(
                            textController: _passwordRepeatKey,
                            name: 'Répétez votre mot de passe',
                            hint: 'Répétez votre mot de passe',
                            backgroundColor: Color(0xff02132B).withOpacity(.03),
                            isSecretInput: true,
                            textInputAction: TextInputAction.done,
                            validator: (String? value) => (value == null ||
                                    value != _passwordKey.value.text)
                                ? 'Mot de passe différent...'
                                : null,
                          ),
                          SampleButton(
                              content: 'Valider',
                              onTap: () async {
                                if (_formKey.currentState?.validate() ??
                                    false) {
                                  if (await _showCGUModal(context) == true) {
                                    final auth = pr.Provider.of<UserViewModel>(
                                        context,
                                        listen: false);
                                    if (!(await auth.signUpWithEmailAndPassword(
                                        _emailKey.value.text,
                                        _passwordKey.value.text,
                                        context))) return;
                                    await showDialog(
                                      context: context,
                                      builder: (ctx) => BoxAlert(
                                          title: 'Vérifiez votre boîte mail',
                                          content:
                                              'Un email de vérification vous a été envoyé à l’adresse ' +
                                                  _emailKey.value.text),
                                    );
                                    await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                CreateProfileView()));
                                    Navigator.pop(context);
                                  } else {
                                    showDialog(
                                      context: context,
                                      builder: (ctx) => BoxAlert(
                                          title: 'Erreur...',
                                          content:
                                              'Vous devez acceptez les conditions générales d\'utilisation.'),
                                    );
                                  }
                                }
                              }),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      ),
    );
  }

  Future<bool> _showCGUModal(BuildContext context) async {
    final rep = await showModalBottomSheet(
      isDismissible: false,
      //enableDrag: false,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(55),
          topRight: Radius.circular(55),
        ),
      ),
      isScrollControlled: true,
      builder: (context) => CGUModalView(),
    );
    return rep ?? false;
  }
}
