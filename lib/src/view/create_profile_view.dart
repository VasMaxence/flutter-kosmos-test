import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_button.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_image_picker.dart';
import 'package:kosmos_flutter_app/src/view_model/auth_provider.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

import 'component/sample_textfield.dart';

class CreateProfileView extends StatelessWidget {
  CreateProfileView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final _imageKey = GlobalKey<SampleImagePickerState>();
  final _firstnameKey = TextEditingController();
  final _nameKey = TextEditingController();

  void dispose() {
    _firstnameKey.dispose();
    _nameKey.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Positioned(
              top: 20,
              left: 20,
              child: GestureDetector(
                child: Icon(
                  Icons.arrow_back_rounded,
                  size: 30,
                  color: Color(0xff02132B),
                ),
                onTap: () => Navigator.pop(context),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
              child: Center(
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Column(
                    children: [
                      Text(
                        'Créez votre profil',
                        maxLines: 3,
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 23,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Color(0xff02132B),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 30),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              SampleTextFormField(
                                textController: _firstnameKey,
                                name: 'Prénom*',
                                hint: 'Ex. John',
                                backgroundColor:
                                    Color(0xff02132B).withOpacity(.03),
                                validator: (String? value) {
                                  if (value == null || value.isEmpty)
                                    return 'Obligatoire';
                                  return null;
                                },
                              ),
                              SampleTextFormField(
                                textController: _nameKey,
                                name: 'Nom*',
                                hint: 'Ex. Doe',
                                backgroundColor:
                                    Color(0xff02132B).withOpacity(.03),
                                validator: (String? value) {
                                  if (value == null || value.isEmpty)
                                    return 'Obligatoire';
                                  return null;
                                },
                              ),
                              SampleImagePicker(
                                key: _imageKey,
                                name: 'Photo de profil*',
                                backgroundColor:
                                    Color(0xff02132B).withOpacity(.03),
                                height: MediaQuery.of(context).size.height * .2,
                              ),
                              SampleButton(
                                content: 'Terminer l\'inscription',
                                onTap: () async {
                                  if (_formKey.currentState != null &&
                                      _formKey.currentState!.validate() &&
                                      _imageKey.currentState?.file != null) {
                                    final auth =
                                        pr.Provider.of<UserViewModel>(context, listen: false);
                                    await auth
                                        .updateUserInformation(
                                      context,
                                      name: _nameKey.value.text,
                                      firstname: _firstnameKey.value.text,
                                      image: _imageKey.currentState?.file,
                                    );
                                    Navigator.pop(context);
                                  }
                                },
                                paddingTop: 30,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Text(
                                  '* Les champs marqués sont obligatoires.',
                                  maxLines: 2,
                                  overflow: TextOverflow.clip,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Poppins',
                                    color: Color(0xff02132B),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
