import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kosmos_flutter_app/src/view/account_view.dart';
import 'package:kosmos_flutter_app/src/view/adding_post_view.dart';
import 'package:kosmos_flutter_app/src/view/component/post_card.dart';
import 'package:kosmos_flutter_app/src/view_model/post_view_model.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:kosmos_flutter_app/src/view/component/top_snackbar/lib/custom_snack_bar.dart';
import 'package:kosmos_flutter_app/src/view/component/top_snackbar/lib/top_snack_bar.dart';
import 'package:provider/provider.dart' as pr;

import 'bottom_bar_view.dart';

class HomeView extends ConsumerWidget {
  HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      floatingActionButton: Container(
        height: 62,
        width: 62,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {
              Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddingPostView()))
                  .then((value) {
                if (value) {
                  showTopSnackBar(
                    context,
                    CustomSnackBar.success(
                      message: "Post Publié !\nVotre poste a bien été publié",
                    ),
                  );
                }
              });
            },
            elevation: 2,
            backgroundColor: Colors.white,
            child: Icon(
              Icons.add_rounded,
              color: Color(0xff02132B),
              size: 35,
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: BottomBarView(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            pr.Consumer<UserViewModel>(
                              builder: (context, user, _) {
                                return Text(
                                  "Bonjour, ${user.user!.displayName} 👋",
                                  overflow: TextOverflow.clip,
                                  maxLines: 1,
                                  style: TextStyle(
                                    color: Color(0xff02132B).withOpacity(.35),
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                );
                              },
                            ),
                            Text(
                              "Fil d'actualités",
                              overflow: TextOverflow.clip,
                              maxLines: 2,
                              style: TextStyle(
                                color: Color(0xff02132B),
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        GestureDetector(
                          child: Container(
                            width: 50,
                            height: 50,
                            clipBehavior: Clip.hardEdge,
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(.2),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: pr.Consumer<UserViewModel>(
                              builder: (context, user, _) {
                                return Image.network(user.user!.photoURL ?? '',
                                    fit: BoxFit.cover);
                              },
                            ),
                          ),
                          onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    AccountView()),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 14),
                      child: Divider(
                        color: Color(0xff02132B).withOpacity(.2),
                      ),
                    ),
                    Text(
                      "Récent",
                      overflow: TextOverflow.clip,
                      maxLines: 1,
                      style: TextStyle(
                        color: Color(0xff02132B),
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              FutureBuilder(
                future: pr.Provider.of<PostViewModel>(context).readPost(),
                builder: (context, doc) {
                  if (doc.hasData) {
                    return Column(
                      children:
                          (doc.data as List<Map<String, dynamic>>).map((e) {
                        return Padding(
                          padding: EdgeInsets.symmetric(vertical: 4),
                          child: PostCard(data: e),
                        );
                      }).toList(),
                    );
                  }
                  return Container();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
