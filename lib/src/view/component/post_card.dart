import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/post_view.dart';
import 'package:kosmos_flutter_app/src/view_model/post_view_model.dart';
import 'package:kosmos_flutter_app/src/view_model/user_view_model.dart';
import 'package:provider/provider.dart' as pr;

class PostCard extends StatelessWidget {
  final Map<String, dynamic> data;

  PostCard({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => PostView(data: data))),
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * .4,
          color: Colors.black38,
          child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Hero(
                tag: data["name"],
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .4,
                  child: Image.network(
                    data["image"],
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .2,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [Colors.black, Colors.transparent],
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        clipBehavior: Clip.hardEdge,
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: Colors.white),
                        child: Image.network(
                          data["user"]["user_image"],
                          fit: BoxFit.cover,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  (data["user"]["user_name"] as String)
                                      .toUpperCase(),
                                  maxLines: 1,
                                  overflow: TextOverflow.clip,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    data["posted_at"].toString(),
                                    maxLines: 1,
                                    overflow: TextOverflow.clip,
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Color(0xff88888B),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * .8,
                              child: Text(
                                data["name"],
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                                style: TextStyle(
                                  fontSize: 11,
                                  color: Color(0xff88888B),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 5,
                right: 5,
                child: GestureDetector(
                  onTap: () async {
                    showCupertinoModalPopup<void>(
                      context: context,
                      builder: (BuildContext context) => CupertinoActionSheet(
                        title: Text('Que souhaitez-vous faire ?'),
                        cancelButton: CupertinoActionSheetAction(
                          child: Text('Annuler'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        actions: [
                          data["user"]["user_id"] ==
                                  pr.Provider.of<UserViewModel>(context,
                                          listen: false)
                                      .user
                                      ?.uid
                              ? CupertinoActionSheetAction(
                                  child: Text('Supprimer la publication'),
                                  onPressed: () async {
                                    pr.Provider.of<PostViewModel>(context,
                                            listen: false)
                                        .deletePost(data["id"]);
                                    Navigator.pop(context);
                                  },
                                  isDestructiveAction: true,
                                )
                              : CupertinoActionSheetAction(
                                  child: Text('Signaler la publication'),
                                  onPressed: () async {
                                    Navigator.pop(context);
                                  },
                                  isDestructiveAction: true,
                                ),
                        ],
                      ),
                    );
                  },
                  child: Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      color: Colors.black26,
                      borderRadius: BorderRadius.circular(35),
                    ),
                    child: Icon(
                      Icons.more_vert_rounded,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
