import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_button.dart';

class WaitingButton extends StatefulWidget {
  final String content;
  final Color backgroundColor;
  final Future Function() onTap;
  final double? width;
  final double? height;
  final double paddingTop;
  final bool defaultActive;

  WaitingButton({
    Key? key,
    required this.content,
    required this.onTap,
    this.backgroundColor = const Color(0xff02132B),
    this.width,
    this.height,
    this.paddingTop = 60,
    this.defaultActive = false,
  }) : super(key: key);

  @override
  WaitingButtonState createState() => WaitingButtonState();
}

class WaitingButtonState extends State<WaitingButton> {
  bool active = false;

  @override
  void initState() {
    active = widget.defaultActive;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SampleButton(
      content: widget.content,
      onTap: () async {
        if (active) widget.onTap();
      },
      backgroundColor:
          active ? Color(0xff02132B) : Color(0xff02132B).withOpacity(.3),
    );
  }

  void updateState() => setState(() => active = true);

  void deleteState() => setState(() => active = false);
}
