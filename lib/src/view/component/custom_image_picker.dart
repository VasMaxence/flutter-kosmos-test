import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dotted_border/dotted_border.dart';
import 'dart:io';

class CustomImagePicker extends StatefulWidget {
  final Function()? onTap;

  CustomImagePicker({
    Key? key,
    this.onTap,
  }) : super(key: key);

  @override
  CustomImagePickerState createState() => CustomImagePickerState();
}

class CustomImagePickerState extends State<CustomImagePicker> {
  ImagePicker imagePicker = ImagePicker();
  XFile? file;

  double _dotSize = 3;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: DottedBorder(
        borderType: BorderType.RRect,
        radius: Radius.circular(7),
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: MediaQuery
              .of(context)
              .size
              .height,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(7),
          ),
          child: Center(
            child: file != null
                ? Image.file(File(file!.path))
                : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.cloud_upload_outlined,
                  color: Colors.grey,
                  size: 40,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8, left: 20, right: 20),
                  child: Text(
                    file?.name ?? "Appuyer pour choisir une photo",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.clip,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 12,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        color: Colors.black,
        dashPattern: [_dotSize, 5],
      ),
      onTap: () async {
        file = await imagePicker.pickImage(source: ImageSource.gallery);
        _dotSize = 0;
        if (file != null && widget.onTap != null) widget.onTap!();
        setState(() {});
      },
    );
  }

  void resetPicker() =>
      setState(() {
        file = null;
        _dotSize = 3;
      });
}
