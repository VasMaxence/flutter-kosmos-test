import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/component/sample_button.dart';

class ActionButton {
  final String name;
  final Function() onTap;
  final bool backgroundWidget;

  ActionButton(
      {required this.name, required this.onTap, this.backgroundWidget = true});
}

class BoxAlert extends StatelessWidget {
  final String title;
  final String content;
  final List<ActionButton>? actions;

  BoxAlert({
    Key? key,
    required this.title,
    required this.content,
    this.actions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * .7,
        height: MediaQuery.of(context).size.width * .7,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(28), color: Colors.white),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 22, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 2,
                child: Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    child: Icon(
                      Icons.close_rounded,
                      size: 30,
                    ),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                    color: Color(0xff02132B),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  content,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    color: Color(0xffA7ADB5),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: actions != null
                      ? actions!
                          .map((e) => Padding(
                                padding: EdgeInsets.symmetric(horizontal: 4),
                                child: SampleButton(
                                  content: e.name,
                                  onTap: () => e.onTap(),
                                  paddingTop: 0,
                                  backgroundColor: e.backgroundWidget
                                      ? Color(0xff02132B)
                                      : Colors.transparent,
                                  width:
                                      MediaQuery.of(context).size.width * .25,
                                ),
                              ))
                          .toList()
                      : [
                          SampleButton(
                            content: 'Fermer',
                            onTap: () async => Navigator.pop(context),
                            paddingTop: 0,
                            width: MediaQuery.of(context).size.width * .4,
                          )
                        ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
