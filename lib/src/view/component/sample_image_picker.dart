import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class SampleImagePicker extends StatefulWidget {
  final double? width;
  final double? height;
  final String name;
  final Color backgroundColor;

  const SampleImagePicker({
    Key? key,
    this.width,
    this.height,
    required this.name,
    this.backgroundColor = Colors.grey,
  }) : super(key: key);

  @override
  SampleImagePickerState createState() => SampleImagePickerState();
}

class SampleImagePickerState extends State<SampleImagePicker> {
  ImagePicker imagePicker = ImagePicker();
  XFile? file;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 7),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              widget.name,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 6),
            child: GestureDetector(
              child: Container(
                width: widget.width ?? MediaQuery.of(context).size.width,
                height:
                    widget.height ?? MediaQuery.of(context).size.height * .3,
                decoration: BoxDecoration(
                  color: widget.backgroundColor,
                  borderRadius: BorderRadius.circular(7),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.cloud_upload_outlined,
                        color: Colors.grey,
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8, left: 20, right: 20),
                        child: Text(
                          file?.name ?? "Appuyer pour choisir une photo",
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: () async {
                file =
                    await imagePicker.pickImage(source: ImageSource.gallery);
                setState(() {

                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
