import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/component/checker.dart';

class SampleChecker extends StatefulWidget {
  final bool initialState;
  final String content;
  final Widget? suffix;

  const SampleChecker({
    Key? key,
    this.initialState = false,
    required this.content,
    this.suffix,
  }) : super(key: key);

  @override
  SampleCheckerState createState() => SampleCheckerState();
}

class SampleCheckerState extends State<SampleChecker> {
  final _checkerKey = GlobalKey<CheckerState>();
  Color textColor = Color(0xff02132B);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checker(key: _checkerKey, initialState: widget.initialState),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text(
              widget.content,
              maxLines: 5,
              overflow: TextOverflow.clip,
              style: TextStyle(
                fontSize: 11,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.bold,
                color: textColor,
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool get state => _checkerKey.currentState?.state ?? false;

  void mustAcceptCGU() => setState(() => textColor = Colors.redAccent);
}
