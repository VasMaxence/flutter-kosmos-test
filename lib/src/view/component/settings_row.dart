import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsRow extends StatefulWidget {
  final Widget? prefix;
  final String name;
  final String desc;
  final bool isSwitcher;
  final Function()? onTap;
  final bool defaultActivated;

  SettingsRow({
    Key? key,
    this.prefix,
    required this.name,
    required this.desc,
    this.isSwitcher = false,
    this.onTap,
    this.defaultActivated = false,
  }) : super(key: key);

  @override
  SettingsRowState createState() => SettingsRowState();
}

class SettingsRowState extends State<SettingsRow> {
  bool value = false;

  @override
  void initState() {
    value = widget.defaultActivated;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: GestureDetector(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 60,
          decoration: BoxDecoration(
            color: Color(0xffF5F5F5).withOpacity(.67),
            borderRadius: BorderRadius.circular(7),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 11),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    widget.prefix != null
                        ? Padding(
                            padding: EdgeInsets.only(right: 15),
                            child: Container(
                              clipBehavior: Clip.hardEdge,
                              width: 37,
                              height: 37,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(37),
                                color: Color(0xff02132B),
                              ),
                              child: widget.prefix,
                            ),
                          )
                        : Container(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width * .6,
                            maxHeight: 40,
                          ),
                          child: Text(
                            widget.name,
                            overflow: TextOverflow.visible,
                            maxLines: 2,
                            style: TextStyle(
                              color: Color(0xff02132B),
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        widget.desc.isNotEmpty
                            ? ConstrainedBox(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  maxHeight: 20,
                                ),
                                child: Text(
                                  widget.desc,
                                  overflow: TextOverflow.fade,
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: Color(0xffBBBBBB),
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ],
                ),
                widget.isSwitcher
                    ? CupertinoSwitch(
                        value: value,
                        onChanged: (value) =>
                            setState(() => this.value = value),
                        activeColor: Color(0xff02132B),
                      )
                    : Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: Color(0xffBBBBBB),
                        size: 15,
                      ),
              ],
            ),
          ),
        ),
        onTap: () => widget.onTap != null ? widget.onTap!() : null,
      ),
    );
  }
}
