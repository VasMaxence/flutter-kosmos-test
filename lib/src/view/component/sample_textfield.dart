import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SampleTextFormField extends StatefulWidget {
  final String name;
  final FocusNode? node;
  final TextEditingController? textController;
  final TextInputType textInputType;
  final Color backgroundColor;
  final Color textInputColor;
  final bool isSecretInput;
  final TextInputAction textInputAction;
  final String hint;
  final String? Function(String?) validator;
  final String? initialValue;

  SampleTextFormField({
    Key? key,
    required this.name,
    this.node,
    this.textController,
    this.textInputType = TextInputType.text,
    this.backgroundColor = Colors.grey,
    this.textInputColor = Colors.black,
    this.isSecretInput = false,
    this.textInputAction = TextInputAction.next,
    required this.hint,
    required this.validator,
    this.initialValue,
  }) : super(key: key);

  @override
  SampleTextFormFieldState createState() => SampleTextFormFieldState();
}

class SampleTextFormFieldState extends State<SampleTextFormField> {
  bool isShowed = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 7),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              widget.name,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 6),
            child: TextFormField(
              initialValue: widget.initialValue,
              keyboardType: widget.textInputType,
              focusNode: widget.node,
              controller: widget.textController,
              obscureText: widget.isSecretInput && !isShowed,
              maxLength: 255,
              maxLines: 1,
              textInputAction: widget.textInputAction,
              validator: (value) => widget.validator(value),
              style: TextStyle(
                color: widget.textInputColor,
              ),
              decoration: InputDecoration(
                fillColor: widget.backgroundColor,
                filled: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                suffixIcon: widget.isSecretInput
                    ? GestureDetector(
                        child: Icon(
                          Icons.remove_red_eye_outlined,
                          color: Colors.grey,
                        ),
                        onTap: () => setState(() => isShowed = !isShowed),
                      )
                    : null,
                counterText: '',
                hintText: widget.hint,
                hintMaxLines: 1,
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(7),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(7),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
