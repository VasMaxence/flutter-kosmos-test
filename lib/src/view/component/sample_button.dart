import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SampleButton extends StatefulWidget {
  final String content;
  final Color backgroundColor;
  final Future Function() onTap;
  final double? width;
  final double? height;
  final double paddingTop;

  SampleButton({
    Key? key,
    required this.content,
    required this.onTap,
    this.backgroundColor = const Color(0xff02132B),
    this.width,
    this.height,
    this.paddingTop = 60,
  }) : super(key: key);

  @override
  _SampleButtonState createState() => _SampleButtonState();
}

class _SampleButtonState extends State<SampleButton> {
  String content = "Veuillez patienter";

  @override
  void initState() {
    content = widget.content;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: widget.paddingTop),
      child: GestureDetector(
        onTap: () async {
          setState(() {
            this.content = "Veuillez patienter";
          });
          await widget.onTap();
          setState(() {
            this.content = widget.content;
          });
        },
        child: Container(
          width: widget.width ?? MediaQuery.of(context).size.width,
          height: widget.height ?? 54,
          decoration: BoxDecoration(
            color: widget.backgroundColor,
            borderRadius: BorderRadius.circular(7),
          ),
          child: Center(
            child: Text(
              this.content,
              style: TextStyle(
                color: widget.backgroundColor == Colors.transparent
                    ? Color(0xff02132B)
                    : Colors.white,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.none,
                fontSize: 18,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
