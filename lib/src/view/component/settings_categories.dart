import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kosmos_flutter_app/src/view/component/settings_row.dart';

class SettingsCategories extends StatelessWidget {
  final String name;
  final List<SettingsRow> settings;

  const SettingsCategories({
    Key? key,
    required this.settings,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Text(
              name,
              overflow: TextOverflow.clip,
              maxLines: 2,
              style: TextStyle(
                color: Color(0xff02132B),
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          ...settings,
        ],
      ),
    );
  }
}
