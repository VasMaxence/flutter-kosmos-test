import 'package:flutter/material.dart';

class Checker extends StatefulWidget {
  final bool initialState;
  final double? width;
  final double? height;
  final Color? backgroundColor;
  final double? borderRadius;
  final Function()? onClick;
  final Widget? child;

  const Checker({
    Key? key,
    this.initialState = false,
    this.width,
    this.height,
    this.backgroundColor,
    this.borderRadius,
    this.onClick,
    this.child,
  }) : super(key: key);

  @override
  CheckerState createState() => CheckerState();
}

class CheckerState extends State<Checker> {
  late bool state;

  @override
  void initState() {
    super.initState();
    state = widget.initialState;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        width: widget.width ?? 30,
        height: widget.height ?? 30,
        decoration: BoxDecoration(
          color: widget.backgroundColor ?? Colors.grey.withOpacity(.3),
          borderRadius: BorderRadius.circular(widget.borderRadius ?? 8),
          border: Border.all(width: .2),
        ),
        child: state
            ? widget.child ??
                Icon(
                  Icons.check_rounded,
                  color: Colors.green,
                  size: widget.width ?? 30,
                )
            : null,
      ),
      onTap: () {
        widget.onClick != null ? widget.onClick!() : null;
        setState(() => state = !state);
      },
    );
  }
}
