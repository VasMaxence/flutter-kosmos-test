import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kosmos_flutter_app/src/view/component/box_alert.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class UserViewModel extends ChangeNotifier {
  FirebaseAuth _auth = FirebaseAuth.instance;
  User? user = FirebaseAuth.instance.currentUser;

  Future<bool> updateUser({String? email}) async {
    final storage = FlutterSecureStorage();
    await _auth.signInWithEmailAndPassword(
        email: email ?? user!.email!,
        password: (await storage.read(key: 'password') ?? ''));
    user = _auth.currentUser;
    notifyListeners();
    return true;
  }

  Future signOut() async {
    await _auth.signOut();
    user = _auth.currentUser;
    notifyListeners();
  }

  Future signInWithEmailAndPassword(
      String email, String password, BuildContext context) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      final storage = new FlutterSecureStorage();
      await storage.write(key: 'password', value: password);
      this.user = _auth.currentUser;
      notifyListeners();
    } on FirebaseAuthException catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: e.message ?? ''),
      );
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(title: 'Erreur...', content: ''),
      );
    }
  }

  Future signUpWithEmailAndPassword(
      String email, String password, BuildContext context) async {
    try {
      final user = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      if (user.user != null && !user.user!.emailVerified)
        user.user!.sendEmailVerification();
      final storage = new FlutterSecureStorage();
      await storage.write(key: 'password', value: password);
      await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      this.user = _auth.currentUser;
      return true;
    } on FirebaseAuthException catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: e.message ?? ''),
      );
      return false;
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: 'Email déjà utiliser.'),
      );
      return false;
    }
  }

  Future resetPassword(String email, BuildContext context) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: e.message ?? ''),
      );
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }

  Future<bool> updateUserInformation(
      BuildContext context, {
        String? name,
        String? firstname,
        String? email,
        String? password,
        XFile? image,
      }) async {
    try {
      final user = _auth.currentUser;
      if (user == null) throw Exception();
      if (email != null) await updateEmail(context, email);
      if (password != null) await updatePassword(context, password);
      if (firstname != null) await user.updateDisplayName(firstname);
      if (image != null)
        await updateProfileImage(
            context, await uploadImageToDatabase(image, context));
      await _auth.currentUser?.reload();
      this.user = _auth.currentUser;
      await this.updateUser();
      notifyListeners();
      return true;
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
    return false;
  }

  Future updateEmail(BuildContext context, String email) async {
    try {
      final user = _auth.currentUser;
      if (user == null) throw Exception();
      await _auth.signInWithEmailAndPassword(
          email: user.email!,
          password: await FlutterSecureStorage().read(key: 'password') ?? '');
      await user.updateEmail(email);
      await user.sendEmailVerification();
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }

  Future<bool> updatePassword(BuildContext context, String password) async {
    try {
      final user = _auth.currentUser;
      if (user == null) throw Exception();
      final pass = await FlutterSecureStorage().read(key: 'password');
      await _auth.signInWithEmailAndPassword(
          email: user.email!,
          password: pass ?? '');
      await user.updatePassword(password);
      await FlutterSecureStorage().write(key: 'password', value: password);
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
      return false;
    }
    return true;
  }

  Future<String?> uploadImageToDatabase(
      XFile? image, BuildContext context) async {
    try {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      final file = File(image!.path);
      final ret =
      await storage.ref('images/profile/' + image.name).putFile(file);
      return ret.ref.getDownloadURL();
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
      return null;
    }
  }

  Future updateProfileImage(BuildContext context, String? path) async {
    try {
      if (path == null) throw Exception();
      final auth = _auth.currentUser;
      await auth!.updatePhotoURL(path);
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }

  Future<bool> checkSamePassword(String? password) async {
    final pass = await FlutterSecureStorage().read(key: 'password');
    return pass == password;
  }

  Future deleteAccount(BuildContext context) async {
    try {
      final pass = await FlutterSecureStorage().read(key: 'password');
      await _auth.signInWithEmailAndPassword(
          email: _auth.currentUser!.email!,
          password: pass ?? '');
      _auth.currentUser?.delete();
      user = _auth.currentUser;
      notifyListeners();
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }
}
