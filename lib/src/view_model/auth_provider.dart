import 'package:kosmos_flutter_app/src/model/authentication_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final authenticationProvider = Provider<AuthenticationModel>((ref) {
  return AuthenticationModel();
});

final authStateProvider = StreamProvider<User?>((ref) {
  return ref.read(authenticationProvider).authStateChange;
});

final userProvider =
    Provider<User?>((ref) => FirebaseAuth.instance.currentUser);
