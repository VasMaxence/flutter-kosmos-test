import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kosmos_flutter_app/src/model/post_firestore_model.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class PostViewModel extends ChangeNotifier {
  Future<bool> addNewPost(XFile image, String desc) async {
    try {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      final file = File(image.path);
      final ret = await storage.ref('images/' + image.name).putFile(file);
      final t = await ret.ref.getDownloadURL();
      await PostFirestoreModel()
          .addPost(FirebaseAuth.instance.currentUser, desc, image.name, t);
      notifyListeners();
    } catch (e) {
      print(e);
      return false;
    }
    return true;
  }

  Future<List<Map<String, dynamic>>?> readPost() async {
    return await PostFirestoreModel().readPost();
  }

  Future deletePost(String id) async {
    await PostFirestoreModel().deletePost(id);
    notifyListeners();
  }
}
