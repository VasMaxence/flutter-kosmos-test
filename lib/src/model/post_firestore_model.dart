import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PostFirestoreModel {
  CollectionReference posts = FirebaseFirestore.instance.collection('posts');

  Future addPost(User? user, String desc, String name, String image) async {
    return await posts
        .add({
          'desc': desc,
          'name': name,
          'image': image,
          'score': 1,
          'posted_ad': DateTime.now(),
          'user': {
            'user_img': user?.photoURL,
            'user_name': user?.displayName,
            'user_id': user?.uid,
          }
        })
        .then((value) => print("Post Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<List<Map<String, dynamic>>?> readPost() async {
    final List<Map<String, dynamic>> list = [];
    await posts.orderBy('posted_ad', descending: true).get().then((value) {
      value.docs.forEach((element) {
        list.add({
          'id': element.id,
          'desc': element["desc"],
          'image': element["image"],
          'name': element["name"],
          'posted_at': DateTime.now()
              .difference((element["posted_ad"] as Timestamp).toDate())
              .inDays,
          'user': {
            'user_name': element["user"]["user_name"],
            'user_image': element["user"]["user_img"],
            'user_id': element["user"]["user_id"],
          }
        });
      });
    });
    return list;
  }

  Future deletePost(String id) async {
    return await posts.doc(id).delete();
  }
}
