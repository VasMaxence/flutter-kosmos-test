import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kosmos_flutter_app/src/view/component/box_alert.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class AuthenticationModel {
  FirebaseAuth _auth = FirebaseAuth.instance;

  Stream<User?> get authStateChange => _auth.authStateChanges();

  Future signInWithEmailAndPassword(
      String email, String password, BuildContext context) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      final storage = new FlutterSecureStorage();
      await storage.write(key: 'password', value: password);
    } on FirebaseAuthException catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: e.message ?? ''),
      );
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(title: 'Erreur...', content: ''),
      );
    }
  }

  Future signUpWithEmailAndPassword(
      String email, String password, BuildContext context) async {
    try {
      final user = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      if (user.user != null && !user.user!.emailVerified)
        user.user!.sendEmailVerification();
      final storage = new FlutterSecureStorage();
      await storage.write(key: 'password', value: password);
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return true;
    } on FirebaseAuthException catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: e.message ?? ''),
      );
      return false;
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: 'Email déjà utiliser.'),
      );
      return false;
    }
  }

  Future resetPassword(String email, BuildContext context) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) =>
            BoxAlert(title: 'Erreur...', content: e.message ?? ''),
      );
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }

  Future signOut() async {
    final storage = new FlutterSecureStorage();
    storage.delete(key: 'password');
    await _auth.signOut();
  }

  Future<bool> updateUserInformation(
    BuildContext context, {
    String? name,
    String? firstname,
    String? email,
    String? password,
    XFile? image,
  }) async {
    try {
      final user = _auth.currentUser;
      if (image != null)
        updateProfileImage(
            context, await uploadImageToDatabase(image, context));

      if (user == null) throw Exception();
      if (email != null) await updateEmail(context, email);
      if (password != null) return await updatePassword(context, password);
      if (firstname != null) await user.updateDisplayName(firstname);
      return true;
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
    return false;
  }

  Future updateEmail(BuildContext context, String email) async {
    try {
      final user = _auth.currentUser;
      if (user == null) throw Exception();
      await _auth.signInWithEmailAndPassword(
          email: user.email!,
          password: await FlutterSecureStorage().read(key: 'password') ?? '');
      await user.updateEmail(email);
      await user.sendEmailVerification();
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }

  Future<bool> updatePassword(BuildContext context, String password) async {
    try {
      final user = _auth.currentUser;
      if (user == null) throw Exception();
      final pass = await FlutterSecureStorage().read(key: 'password');
      await _auth.signInWithEmailAndPassword(
          email: user.email!,
          password: pass ?? '');
      await user.updatePassword(password);
      await FlutterSecureStorage().write(key: 'password', value: password);
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
      return false;
    }
    return true;
  }

  Future<String?> uploadImageToDatabase(
      XFile? image, BuildContext context) async {
    try {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      final file = File(image!.path);
      final ret =
          await storage.ref('images/profile/' + image.name).putFile(file);
      return ret.ref.getDownloadURL();
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
      return null;
    }
  }

  Future updateProfileImage(BuildContext context, String? path) async {
    try {
      if (path == null) throw Exception();
      final auth = _auth.currentUser;
      auth?.updatePhotoURL(path);
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }

  Future<bool> checkSamePassword(String? password) async {
    final pass = await FlutterSecureStorage().read(key: 'password');
    return pass == password;
  }

  Future deleteAccount(BuildContext context) async {
    try {
      final pass = await FlutterSecureStorage().read(key: 'password');
      await _auth.signInWithEmailAndPassword(
          email: _auth.currentUser!.email!,
          password: pass ?? '');
      _auth.currentUser?.delete();
    } catch (e) {
      await showDialog(
        context: context,
        builder: (ctx) => BoxAlert(
            title: 'Erreur...', content: "Veuillez réessayer s'il vous plait."),
      );
    }
  }
}
